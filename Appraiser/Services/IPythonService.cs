﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Services
{
    public interface IPythonService
    {
        public string RunPythonScript(List<string> argv, string scriptName);
    }
}
