﻿using Appraiser.Data.Entities;
using Appraiser.Data.Models.Request;
using Appraiser.Data.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Services
{
    public class AppraiserServiceMySql : IAppraiserService
    {
        private AppraiserContext _appraiserContext { get; set; }
        private IPythonService _pythonService { get; set; }
        public AppraiserServiceMySql(AppraiserContext appraiserContext, IPythonService pythonService)
        {
            _appraiserContext = appraiserContext;
            _pythonService = pythonService;
        }

        public async Task<AppraisalResponseDTO> RequestAppraisal(AppraisalRequestDTO r)
        {
            AppraisalRequest entity = new AppraisalRequest() {
                AdjPropTypes = r.AdjPropTypes,
                CurrentMonthRental = r.CurrentMonthRental,
                EmailId = r.EmailId,
                FamilyCharityRent = r.FamilyCharityRent,
                FamilyCharityRentCmnts = r.FamilyCharityRentCmnts,
                NumUnits1Br = r.NumUnits1Br,
                NumUnits2Br = r.NumUnits2Br,
                NumUnits3BrOrMore = r.NumUnits3BrOrMore,
                NumUnitsStudioConv = r.NumUnitsStudioConv,
                NumUnits = r.NumUnits1Br + r.NumUnits2Br + r.NumUnits3BrOrMore + r.NumUnitsStudioConv,
                NumFloors = r.NumFloors,
                OriginalFinancingAmount = r.OriginalFinancingAmount,
                OriginalPurchasePrice = r.OriginalPurchasePrice,
                ParcelNumber = r.ParcelNumber,
                PropAdditionalInfo = r.PropAdditionalInfo,
                PropCity = r.PropCity,
                PropCounty = r.PropCounty,
                PropState = r.PropState,
                PropStrName = r.PropStrName,
                PropStrNumber = r.PropStrNumber,
                NetOperatingIncome = r.NetOperatingIncome,
                PropSubCategory = r.PropSubCategory,
                PropSubCategoryOther = r.PropSubCategoryOther,
                PropType = r.PropType,
                PropZipCode = r.PropZipCode,
                RemodelAddition = r.RemodelAddition,
                RemodelAdditionYearRange = r.RemodelAdditionYearRange,
                RentLow = r.RentLow,
                RepairRemodel = r.RepairRemodel,
                RepairRemodelCmnts = r.RepairRemodelCmnts,
                VacRateCmnts = r.VacRateCmnts,
                ValuationId = r.ValuationId,
                YearBuilt = r.YearBuilt,
                NumParkingCovered = r.NumParkingCovered,
                NumParkingGarage = r.NumParkingGarage,
                NumParkingOpen = r.NumParkingOpen,
                NumParkingSecureGarage = r.NumParkingSecureGarage,
                TotalParkingSpaces = r.NumParkingOpen + r.NumParkingCovered + r.NumParkingGarage + r.NumParkingSecureGarage,
                SaleDate = DateTime.UtcNow,
                NumBedrooms = r.NumUnits1Br + r.NumUnits2Br*2 + r.NumUnits3BrOrMore*3,
                Latitude = r.Latitude,
                Longitude = r.Longitude, 
                BuildingSqFt = r.BuildingSqFt,
                PropAdditionalAddress = r.PropAdditionalAddress
            };

            _appraiserContext.Add(entity);
            _appraiserContext.SaveChanges();

            // var result = _pythonService.RunPythonScript(new List<string>() { }, "helloworld.py");
            return await Task.FromResult(new AppraisalResponseDTO() { Status = "Ok", StatusCode = 200 });
        }
    }
}
