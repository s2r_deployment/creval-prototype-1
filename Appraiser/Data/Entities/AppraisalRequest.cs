﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Appraiser.Data.Entities
{
    public class AppraisalRequest
    {
        [Key]
        public int Id { get; set; }

        public int ValuationId { get; set; }

        [MaxLength(45)]
        public string EmailId { get; set; }

        [Required]
        [MaxLength(45)]
        public string PropType { get; set; }

        [MaxLength(45)]
        public string ParcelNumber { get; set; }

        [Required]
        [MaxLength(250)]
        public string AdjPropTypes { get; set; }

        [Required]
        public int NumParkingOpen { get; set; }

        [Required]
        public int NumParkingCovered { get; set; }

        [Required]
        public int NumParkingGarage { get; set; }

        [Required]
        public int NumParkingSecureGarage { get; set; }

        [Required]
        public int NumUnits { get; set; }

        [Required]
        [MaxLength(45)]
        public string PropSubCategory { get; set; }

        [MaxLength(45)]
        public string PropSubCategoryOther { get; set; }

        [Required]
        public int TotalParkingSpaces { get; set; }
               
        [Required]
        public int YearBuilt { get; set; }

        [Required]
        [MaxLength(10)]
        public string RemodelAddition { get; set; }

        [MaxLength(15)]
        public string RemodelAdditionYearRange { get; set; }
        
        public decimal OriginalPurchasePrice { get; set; }
        
        public decimal OriginalFinancingAmount { get; set; }

        [Required]
        [MaxLength(45)]
        public string PropStrName { get; set; }

        [Required]        
        public int PropStrNumber { get; set; }

        [Required]
        [MaxLength(45)]
        public string PropCounty { get; set; }

        [Required]
        [MaxLength(45)]
        public string PropCity { get; set; }

        [Required]
        [MaxLength(2)]
        public string PropState { get; set; }

        [Required]
        [MaxLength(10)]
        public string PropZipCode { get; set; }

        [Required]
        public decimal CurrentMonthRental { get; set; }

        [Required]
        [MaxLength(10)]
        public string RentLow { get; set; }

        [Required]
        [MaxLength(10)]
        public string FamilyCharityRent { get; set; }

        [MaxLength(250)]
        public string FamilyCharityRentCmnts { get; set; }

        [Required]
        [MaxLength(10)]
        public string PropAdditionalAddress { get; set; }

        [Required]
        [MaxLength(45)]
        public string VacRateCmnts { get; set; }

        [Required]
        [MaxLength(10)]
        public string RepairRemodel { get; set; }

        [MaxLength(250)]
        public string RepairRemodelCmnts { get; set; }

        [MaxLength(250)]
        public string PropAdditionalInfo { get; set; }

        [Required]
        public int NumUnitsStudioConv { get; set; }

        [Required]
        public int NumUnits1Br { get; set; }

        [Required]
        public int NumUnits2Br { get; set; }

        [Required]
        public int NumUnits3BrOrMore { get; set; }

        [Required]
        public decimal BuildingSqFt { get; set; }

        [Required]
        public int NumFloors { get; set; }


        [Required]
        public int NumBedrooms { get; set; }

        [Required]
        public DateTime SaleDate { get; set; }

        [Required]
        public decimal Latitude { get; set; }

        [Required]
        public decimal Longitude { get; set; }

        [Required]
        [MaxLength(10)]
        public string NetOperatingIncome { get; set; }

        public int StatusCode { get; set; }

        public string Status { get; set; }

    }
}
