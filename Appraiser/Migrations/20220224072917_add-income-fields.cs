﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class addincomefields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IsLevel1Sufficient",
                table: "AppResponses",
                type: "varchar(10)",
                maxLength: 10,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<decimal>(
                name: "GrossIncome",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NetOperatingIncome",
                table: "AppRequests",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsLevel1Sufficient",
                table: "AppResponses");

            migrationBuilder.DropColumn(
                name: "GrossIncome",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NetOperatingIncome",
                table: "AppRequests");
        }
    }
}
