﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class updatemodelunitssqft : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BuildingSqFt",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumUnits1Br",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumUnits2Br",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumUnits3BrOrMore",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumUnitsStudioConv",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BuildingSqFt",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumUnits1Br",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumUnits2Br",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumUnits3BrOrMore",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumUnitsStudioConv",
                table: "AppRequests");
        }
    }
}
