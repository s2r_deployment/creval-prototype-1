﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

namespace Appraiser.Migrations
{
    public partial class AddPropertyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PropertyData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    propertyAddress = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    propertyType = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    saleCond = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    dateSale = table.Column<DateTime>(type: "datetime", nullable: false),
                    priceSale = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    sfLand = table.Column<int>(type: "int", nullable: false),
                    sfBldg = table.Column<int>(type: "int", nullable: false),
                    yrBlt = table.Column<int>(type: "int", nullable: false),
                    parkingSp = table.Column<int>(type: "int", nullable: false),
                    units = table.Column<int>(type: "int", nullable: false),
                    bldgClass = table.Column<string>(type: "varchar(10)", maxLength: 10, nullable: true),
                    starRating = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    grossRentMultiplier = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    noi = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    expTot = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    cap = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    cond = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    zeroBr = table.Column<int>(type: "int", nullable: false),
                    oneBr = table.Column<int>(type: "int", nullable: false),
                    twoBr = table.Column<int>(type: "int", nullable: false),
                    threeBr = table.Column<int>(type: "int", nullable: false),
                    levels = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PropertyData", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PropertyData");
        }
    }
}
