﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Appraiser.Migrations
{
    public partial class updateparkingfields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumParkingCovered",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumParkingGarage",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumParkingOpen",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "NumParkingSecureGarage",
                table: "AppRequests",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumParkingCovered",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumParkingGarage",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumParkingOpen",
                table: "AppRequests");

            migrationBuilder.DropColumn(
                name: "NumParkingSecureGarage",
                table: "AppRequests");
        }
    }
}
