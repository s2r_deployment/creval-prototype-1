﻿using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

namespace Appraiser.Migrations
{
    public partial class UpdateRespModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppResponses",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    AppraisalRequestId = table.Column<int>(type: "int", nullable: false),
                    ValuationId = table.Column<int>(type: "int", nullable: false),
                    ValuationAmount = table.Column<decimal>(type: "decimal(18, 2)", nullable: false),
                    ReliabilityScore = table.Column<int>(type: "int", nullable: false),
                    RecommendationText = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    CredConfInterval = table.Column<string>(type: "varchar(45)", maxLength: 45, nullable: true),
                    StatusCode = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AppResponses_AppRequests_AppraisalRequestId",
                        column: x => x.AppraisalRequestId,
                        principalTable: "AppRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AppResponses_AppraisalRequestId",
                table: "AppResponses",
                column: "AppraisalRequestId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppResponses");
        }
    }
}
